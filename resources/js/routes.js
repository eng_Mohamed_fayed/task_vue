import AllEvent from './components/AllEvent.vue';
import CreateEvent from './components/CreateEvent.vue';
import EditEvent from './components/EditEvent.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllEvent
    },
    {
        name: 'create',
        path: '/create',
        component: CreateEvent
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditEvent
    }
];


