<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Event;
use Auth;

class EventController extends Controller
{
    /**
     * Get all event by guard admin
     *
     * @return array
     */
    public function index()
    {
        if(Auth::guard('admin')->check()) {
            $events = Event::get()->toArray();
        }else{
            $events = Event::Organizer()->get()->toArray();
        }
        return array_reverse($events);
    }

    /**
     * store event by guard
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if(Auth::guard('admin')->check()) {
            $status = "Approved";
            $user_id = auth('admin')->user()->id;
        }else{
            $status = "Pending";
            $user_id = auth()->user()->id;

        }
        $event = new Event([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user_id' => $user_id,
            'status' => $status,
        ]);
        $event->save();

        return response()->json('Event created!');
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $event = Event::find($id);
        return response()->json($event);
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request)
    {
        $event = Event::find($id);
        $event->update($request->all());

        return response()->json('Event updated!');
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();

        return response()->json('Event deleted!');
    }
}
