<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class AdminController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function adminLogin()
    {
        return view('auth.adminLogin');
    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function savelogin(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::guard('admin')->attempt($credentials)) {
            return redirect()->intended('/');
        }

        return redirect('/admin/login')->with('error', 'Oppes! You have entered invalid credentials');
    }

    /**
     * @param Request $request
     * @return Application|Redirector
     */
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }
}
