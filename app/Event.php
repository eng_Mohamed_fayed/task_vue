<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Event extends Model
{
    protected $fillable = [
        'name',
        'user_id',
        'description',
        'status'
    ];

    public function scopeOrganizer($query)
    {
        return $query->where('user_id', Auth::guard('web')->user()->id);
    }

}
